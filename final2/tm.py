import re
import sys

INPUT_PATH = "input.txt"
OUTPUT_PATH = "output.txt"

transitions = {
    ("q0", "0", "Z"): ("q0", "Z", ("S", "R")),
    ("q0", "1", "Z"): ("q0", "Z", ("S", "R")),
    ("q0", "0", "_"): ("q0", "0", ("R", "R")),
    ("q0", "1", "_"): ("q0", "1", ("R", "R")),
    ("q0", "#", "_"): ("q1", "_", ("R", "L")),

    ("q1", "1", "1"): ("q1", "1", ("R", "L")),
    ("q1", "0", "0"): ("q1", "0", ("R", "L")),
    ("q1", "1", "0"): ("q1", "0", ("R", "L")),
    ("q1", "0", "1"): ("q1", "1", ("R", "L")),
    ("q1", "_", "Z"): ("q2", "Z", ("L", "S")),
    ("q1", "_", "1"): ("q4", "1", ("S", "S")),
    ("q1", "_", "0"): ("q4", "0", ("S", "S")),

    ("q2", "0", "Z"): ("q2", "Z", ("L", "S")),
    ("q2", "1", "Z"): ("q2", "Z", ("L", "S")),
    ("q2", "#", "Z"): ("q3", "Z", ("R", "R")),

    ("q3", "1", "1"): ("q3", "1", ("R", "R")),
    ("q3", "0", "0"): ("q3", "0", ("R", "R")),
    ("q3", "0", "1"): ("q4", "1", ("S", "S"))
}

final_state = "q4"


def read_first_line(path: str) -> str:
    """
    Read the first line from the file
    delete blank symbols and return resulting string
    """
    with open(path, "r", encoding="utf-8") as f:
        data = f.readline().strip()
    return data.replace(" ", "")


def check_validity(data: str) -> bool:
    """Check input string validity"""
    return bool(re.match(r"^(0+|1[01]*)#(0+|1[01]*)$", data))


def write_output(path: str, data: list) -> None:
    """Write strings in the list to file"""
    with open(path, "w") as f:
        f.write("\n".join(data))


def main(transitions: dict, final_state: str):
    """
    I am realized TM through saving transitions to dictionary
    and I am just walking between states, saving state into variables
    and generating configuration strings

    I am used many try-catch blocks for easy avoid empty strings in left or right part of tape index
    and empty symbols in string and memory
    """
    string = read_first_line(INPUT_PATH)

    if not check_validity(string):
        write_output(OUTPUT_PATH, ["Invalid input"])
        sys.exit()

    memory_tape = ["Z"]
    data = ["q0, ^{}, ^Z".format(string), "q0, ^{}, Z^".format(string)]
    memory_tape_index = 1
    string_tape_index = 0
    current_state = "q0"

    deltas = {
        "R": 1,
        "S": 0,
        "L": -1
    }

    while True:
        try:
            current_mem_element = memory_tape[memory_tape_index]
        except IndexError:
            current_mem_element = "_"

        try:
            next_symbol_from_string = string[string_tape_index]
        except IndexError:
            next_symbol_from_string = "_"

        current = (current_state, next_symbol_from_string, current_mem_element)

        try:
            next_state, mem_element_to_write, directions = transitions[current]
        except KeyError:
            is_final = current_state == final_state
            if not is_final:
                data.append(data[-1])
            break

        string_direction, mem_direction = directions
        string_tape_index += deltas[string_direction]

        if mem_element_to_write != "_":
            try:
                memory_tape[memory_tape_index] = mem_element_to_write
            except IndexError:
                memory_tape.append(mem_element_to_write)

        memory_tape_index += deltas[mem_direction]

        try:
            left_string = string[:string_tape_index]
        except IndexError:
            left_string = ""

        try:
            right_string = string[string_tape_index:]
        except IndexError:
            right_string = ""

        mem_as_string = "".join(memory_tape)

        try:
            left_mem = mem_as_string[:memory_tape_index]
        except IndexError:
            left_mem = ""

        try:
            right_mem = mem_as_string[memory_tape_index:]
        except IndexError:
            right_mem = ""

        if current_state == final_state:
            is_final = True
            break

        config = "{}, {}^{}, {}^{}".format(next_state, left_string, right_string, left_mem, right_mem)
        data.append(config)

        current_state = next_state

    data.append("YES" if is_final else "NO")
    write_output(OUTPUT_PATH, data)


if __name__ == "__main__":
    main(transitions, final_state)
