import re
import sys
from collections import defaultdict, Counter


class FSA:
    """
    FSA class, which allow us to load, check FSA errors,
    generate regular expression from FSA and save it to file
    Author - Abuzyar Tazetdinov
    """
    ERRORS = {
        1: "E1: A state '{}' is not in the set of states",
        2: "E2: Some states are disjoint",
        3: "E3: A transition '{}' is not represented in the alphabet",
        4: "E4: Initial state is not defined",
        5: "E5: Input file is malformed",
        6: "E6: FSA is nondeterministic"
    }

    FILE_CHECK_REGEX = re.compile(
        r"^ *states=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *alpha=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *init\.st=\[\w*\] *\n"
        r" *fin\.st=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *trans=\[((\w+>\w+>\w+)?|(\w+>\w+>\w+(,\w+>\w+>\w+)+))\][\n ]*$"
    )

    def __init__(self):
        self.states = []  # all states in FSA
        self.alphabet = set()  # supported alphabet
        self.init_state = ""  # initial state
        self.final_states = []  # final states

        # transitions - dict with key is starting state
        # and value is list containing tuples (alpha, ending state)
        # {s1: [(a, s2),..],..}
        self.transitions = {}

        self.warnings = set()  # container for warning saving if they appear
        self.reachable_states = set()  # stores states reachable from the initial state
        self.unreachable_states = set()  # stores states unreachable from the initial state

        # counts every nondeterministic transition from each state
        # for example: trans=[s1>a>s2, s1>a>s3, s1>a>s4]
        # in this case self.nondeterministic_transitions_count[s1]
        # will return 2
        self.nondeterministic_transitions_count = Counter()

        self.re = ""  # regular expression from this FSA

    @staticmethod
    def __extract_line(line: str) -> list:
        """Extract raw data from line"""
        return line.strip()[line.find("[") + 1:-1].split(",")

    @staticmethod
    def __write_result(status: str) -> None:
        """Write result to file"""
        with open("result.txt", "w", encoding="utf-8") as f:
            f.write(status)

    def __raise_error(self, n: int, s: str = None) -> None:
        """Write error to file and close the program"""
        if n in {1, 3}:
            self.__write_result("Error:\n{}".format(self.ERRORS[n].format(s)))
        else:
            self.__write_result("Error:\n{}".format(self.ERRORS[n]))
        sys.exit()

    def load(self) -> None:
        """Load FSA from file, check some errors"""
        with open("fsa.txt", "r", encoding="utf-8") as f:
            file_content = f.read()

        if not re.match(FSA.FILE_CHECK_REGEX, file_content):  # Checking syntax errors in the file
            self.__raise_error(5)

        lines = file_content.split("\n")  # Splitting file to lines
        self.states = self.__extract_line(lines[0])
        self.alphabet = set(self.__extract_line(lines[1]))

        self.init_state = self.__extract_line(lines[2])[0]
        self.__check_init_state()

        self.final_states = self.__extract_line(lines[3])
        self.__check_final_states()

        self.__parse_and_check_transitions(self.__extract_line(lines[4]))

    def __check_init_state(self) -> None:
        """Check initial state"""
        if self.init_state == "":
            self.__raise_error(4)  # Not defined

        if self.init_state not in self.states:
            self.__raise_error(1, self.init_state)  # Not in states

    def __check_final_states(self) -> None:
        """Check final state"""
        if self.final_states[0] != "":  # if final states defined
            for state in self.final_states:
                if state not in self.states:
                    self.__raise_error(1, state)  # Not in states

    def __parse_and_check_transitions(self, raw_transitions: list) -> None:
        """Parse raw transitions string and check them"""
        self.transitions = defaultdict(lambda: [])
        if raw_transitions[0] != "":
            for transition in raw_transitions:
                s1, a, s2 = transition.split(">")
                if s1 not in self.states:
                    self.__raise_error(1, s1)
                if s2 not in self.states:
                    self.__raise_error(1, s2)
                if a not in self.alphabet:
                    self.__raise_error(3, a)
                self.transitions[s1].append((a, s2))

    def __find_unreachable_states(self, state: str) -> None:
        """
        Recursive function
        Add all reachable from the given state states to self.reachable_states
        """
        if state not in self.reachable_states:
            self.reachable_states.add(state)
            for pair in self.transitions[state]:
                self.__find_unreachable_states(pair[1])

    def __check_unreachable_and_disjoint_states(self) -> None:
        # we add all reachable from the init state states to self.reachable_states
        self.__find_unreachable_states(self.init_state)

        self.unreachable_states = set(self.states) - self.reachable_states

        if self.unreachable_states:  # if we have unreachable states
            # Checking disjoint states
            for s1 in self.unreachable_states:
                flag = True
                for a, s2 in self.transitions[s1]:
                    if s1 != s2:  # if state is unreachable and doesn't contain transitions to another states
                        # then this state is disjoint
                        flag = False
                        break
                if flag:
                    self.__raise_error(2)

    def __get_nondeterministic_transitions_count(self) -> None:
        """Counts all nondeterministic transitions from each state"""
        for s1, pairs in self.transitions.items():
            used_alphas = set()
            for a, s2 in pairs:
                if a in used_alphas:
                    self.nondeterministic_transitions_count[s1] += 1
                else:
                    used_alphas.add(a)

    def __check_nondeterminism(self) -> None:
        """Check FSA is nondeterministic or not"""
        # We just check our counter
        # if counter is not empty - we have nondeterministic states - then we add warning
        if self.nondeterministic_transitions_count:
            self.__raise_error(6)

    def check(self) -> None:
        """Checking FSA"""
        self.__get_nondeterministic_transitions_count()
        self.__check_unreachable_and_disjoint_states()  # E2
        self.__check_nondeterminism()  # E6

    def __first_iteration(self, state_indexes: dict) -> dict:
        """
        First iteration for Kleene algorithm
        Calculates regexes with -1'st degree for each pair of states
        :param state_indexes: mapping from states to int
        :return: -1'st degree regexes
        """
        regexes = defaultdict(str)  # dict: (s1, s2, level): regex
        for s1 in self.states:
            for a, s2 in self.transitions[s1]:
                i = (state_indexes[s1], state_indexes[s2], -1)
                regexes[i] += a if not regexes[i] else "|" + a
        for s1 in self.states:
            i = (state_indexes[s1], state_indexes[s1], -1)
            regexes[i] += "eps" if not regexes[i] else "|eps"
        for s1 in self.states:
            for s2 in self.states:
                i = (state_indexes[s1], state_indexes[s2], -1)
                if not regexes[i]:
                    regexes[i] = "{}"
        return regexes

    def __second_iteration(self, regexes: dict, state_indexes: dict) -> None:
        """
        Calculate regular expressions for 0..n degrees and add to regexes
        Change global variable passed via reference
        :param regexes: -1'st degree regexes calculated in first iteration
        :param state_indexes: mapping from states to int
        """
        for k in range(len(self.states) + 1):
            for s1 in self.states:
                for s2 in self.states:
                    i = state_indexes[s1]
                    j = state_indexes[s2]
                    index = (i, j, k)
                    regexes[index] = "({})({})*({})|({})".format(regexes[(i, k, k - 1)], regexes[(k, k, k - 1)],
                                                                 regexes[(k, j, k - 1)], regexes[(i, j, k - 1)])

    def __get_state_indexes(self) -> dict:
        """
        Maps states to integers and return mapping
        :return: mapping from states to int
        """
        state_indexes = {}  # dict state: index
        for i, s in enumerate(self.states):
            state_indexes[s] = i
        return state_indexes

    def __get_final_states_indexes(self, state_indexes: dict) -> set:
        """
        Maps final states to integers using mapper
        :return: mapping from final states to int
        """
        final_states = set()
        for f in self.final_states:
            final_states.add(state_indexes[f])
        return final_states

    def __set_re(self, state_indexes: dict, final_states: set, regexes: dict):
        """
        Assign self.re with possible regexes union
        :param state_indexes: mapping from states to int
        :param final_states: mapping from final states to int
        :param regexes: regexes for all iterations
        """
        res = []
        for i, j, k in sorted(regexes.keys()):
            if i == state_indexes[self.init_state] and j in final_states and k == len(self.states) - 1:
                res.append(regexes[(i, j, k)])
        self.re = "|".join(res)

    def regex(self):
        """Transforms FSA to regex using Kleene algorithm"""
        if self.final_states[0] == '':  # if there are no final states
            self.re = "{}"
            return

        # We get mappings from states to int
        state_indexes = self.__get_state_indexes()
        final_states = self.__get_final_states_indexes(state_indexes)

        # Generating regular expression
        regexes = self.__first_iteration(state_indexes)
        self.__second_iteration(regexes, state_indexes)
        self.__set_re(state_indexes, final_states, regexes)

    def save(self):
        """Save FSA regex if no errors"""
        self.__write_result(self.re)


def main():
    fsa = FSA()  # create FSA object
    fsa.load()  # load FSA from file (we check some errors while loading)
    fsa.check()  # check E2, E6
    fsa.regex()  # generate regex from FSA
    fsa.save()  # save regex


if __name__ == "__main__":
    main()
