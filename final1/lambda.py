import re

INPUT_PATH = "input.txt"
OUTPUT_PATH = "output.txt"


def read_first_line(path: str) -> str:
    """
    Read the first line from the file
    delete blank symbols and return resulting string
    """
    with open(path, "r", encoding="utf-8") as f:
        data = f.readline().strip()
    return data.replace(" ", "")


def write_output(path: str, data: list) -> None:
    """Write strings in the list to file"""
    with open(path, "w") as f:
        f.write("\n".join(data))


def check_balanced_parenthesis(lambda_expression: str) -> bool:
    """Check balanced parenthesis in string"""
    stack = []
    for s in lambda_expression:
        if s == "(":
            stack.append("(")
            continue
        if s == ")":
            if not stack:
                return False
            stack.pop()

    return not stack


def get_right_brace_index(lambda_expression: str) -> int:
    """
    Find and return the index of the closing bracket
    of first parenthesis group
    """
    stack = []
    last = -1
    flag = False
    for i, s in enumerate(lambda_expression):
        if s == "(":
            if flag:
                break
            stack.append("(")
            continue
        if s == ")":
            if not stack:
                break
            stack.pop()
            if not stack:
                flag = True
            last = i
    return last


def check_lambda_expression(lambda_expression: str) -> bool:
    """
    Check is lambda expression defined by grammar
    lambda ::= var | (lambda)lambda | \var.lambda
    """
    if not lambda_expression:
        return False

    if lambda_expression.endswith(")"):
        return False

    if re.match(r"^\w+$", lambda_expression):
        return True

    if re.match(r"^\\\w+\.", lambda_expression):
        return check_lambda_expression(lambda_expression[lambda_expression.find(".") + 1:])

    if lambda_expression.startswith("("):
        right_index = get_right_brace_index(lambda_expression)
        try:
            left = lambda_expression[1:right_index]
            right = lambda_expression[right_index + 1:]
        except IndexError:
            return False
        return check_lambda_expression(left) and check_lambda_expression(right)

    return False


def count_redexes(lambda_expression: str) -> int:
    """
    Count and return beta-redexes at the first step
    in lambda expression without beta-reduction
    """
    return lambda_expression.count("(\\")


def main():
    lambda_expression = read_first_line(INPUT_PATH)

    if check_balanced_parenthesis(lambda_expression) and check_lambda_expression(lambda_expression):
        write_output(OUTPUT_PATH, ["YES", str(count_redexes(lambda_expression))])
    else:
        write_output(OUTPUT_PATH, ["NO"])


if __name__ == "__main__":
    main()
