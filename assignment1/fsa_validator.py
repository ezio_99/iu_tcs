import re
import sys
from collections import defaultdict, Counter


class FSA:
    """
    FSA class, which allow us to load, check FSA errors and warnings, save checking results
    Author - Abuzyar Tazetdinov
    """
    ERRORS = {
        1: "E1: A state '{}' is not in the set of states",
        2: "E2: Some states are disjoint",
        3: "E3: A transition '{}' is not represented in the alphabet",
        4: "E4: Initial state is not defined",
        5: "E5: Input file is malformed"
    }
    WARNINGS = {
        1: "W1: Accepting state is not defined",
        2: "W2: Some states are not reachable from the initial state",
        3: "W3: FSA is nondeterministic"
    }

    FILE_CHECK_REGEX = re.compile(
        r"^ *states=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *alpha=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *init\.st=\[\w*\] *\n"
        r" *fin\.st=\[(\w*|\w+(,\w+)+)\] *\n"
        r" *trans=\[((\w+>\w+>\w+)?|(\w+>\w+>\w+(,\w+>\w+>\w+)+))\][\n ]*$"
    )

    def __init__(self):
        self.states = None        # all states in FSA  - set
        self.alphabet = None      # supported alphabet - set
        self.init_state = None    # initial state      - list
        self.final_states = None  # final states       - list
        self.transitions = None   # transitions        - dict with key is starting state
                                  #                      and value is list containing tuples (alpha, ending state)
                                  #                      {s1: [(a, s2),..],..}
        self.warnings = set()     # container for warning saving if they appear - set
        self.is_complete = True   # flag representing FSA completeness
        self.reachable_states = None    # set which stores states reachable from the initial state
        self.unreachable_states = None  # set which stores states unreachable from the initial state
        self.nondeterministic_transitions_count = None  # counts every nondeterministic transition from each state
                                                        # for example: trans=[s1>a>s2, s1>a>s3, s1>a>s4]
                                                        # in this case self.nondeterministic_transitions_count[s1]
                                                        # will return 2
                                                        # this field type is collections.Counter

    @staticmethod
    def extract_line(line):
        """Extract raw data from line"""
        return line.strip()[line.find("[") + 1:-1].split(",")

    @staticmethod
    def write_status(status):
        """Write checking status to file"""
        with open("result.txt", "w", encoding="utf-8") as f:
            f.write(status)

    def raise_error(self, n, s=None):
        """Write error to file and close the program"""
        if n in {1, 3}:
            self.write_status("Error:\n{}".format(self.ERRORS[n].format(s)))
        else:
            self.write_status("Error:\n{}".format(self.ERRORS[n]))
        sys.exit()

    def load(self):
        """Load FSA from file, check some errors"""
        with open("fsa.txt", "r", encoding="utf-8") as f:
            file_content = f.read()

        if not re.match(FSA.FILE_CHECK_REGEX, file_content):  # Checking syntax errors in the file
            self.raise_error(5)

        lines = file_content.split("\n")  # Splitting file to lines
        self.states = set(self.extract_line(lines[0]))
        self.alphabet = set(self.extract_line(lines[1]))

        self.init_state = self.extract_line(lines[2])[0]
        self.check_init_state()

        self.final_states = self.extract_line(lines[3])
        self.check_final_states()

        self.parse_and_check_transitions(self.extract_line(lines[4]))

    def check_init_state(self):
        """Check initial state"""
        if self.init_state == "":
            self.raise_error(4)  # Not defined

        if self.init_state not in self.states:
            self.raise_error(1, self.init_state)  # Not in states

    def check_final_states(self):
        """Check final state"""
        if self.final_states[0] != "":  # if final states defined
            for state in self.final_states:
                if state not in self.states:
                    self.raise_error(1, state)  # Not in states
        else:  # if final state is not defined
            self.warnings.add(1)

    def parse_and_check_transitions(self, raw_transitions):
        """Parse raw transitions string and check them"""
        self.transitions = defaultdict(lambda: [])
        if raw_transitions[0] != "":
            for transition in raw_transitions:
                s1, a, s2 = transition.split(">")
                if s1 not in self.states:
                    self.raise_error(1, s1)
                if s2 not in self.states:
                    self.raise_error(1, s2)
                if a not in self.alphabet:
                    self.raise_error(3, a)
                self.transitions[s1].append((a, s2))

    def find_unreachable_states(self, state):
        """
        Recursive function
        Add all reachable from the given state states to self.reachable_states
        """
        if state not in self.reachable_states:
            self.reachable_states.add(state)
            for pair in self.transitions[state]:
                self.find_unreachable_states(pair[1])

    def check_unreachable_and_disjoint_states(self):
        self.reachable_states = set()
        self.find_unreachable_states(self.init_state)  # we add all reachable from the init state states
                                                       # to self.reachable_states
        self.unreachable_states = self.states - self.reachable_states

        if self.unreachable_states:  # if we have unreachable states
            self.warnings.add(2)

            # Checking disjoint states
            for s1 in self.unreachable_states:
                flag = True
                for a, s2 in self.transitions[s1]:
                    if s1 != s2:       # if state is unreachable and doesn't contain transitions to another states
                                       # then this state is disjoint
                        flag = False
                        break
                if flag:
                    self.raise_error(2)

    def get_nondeterministic_transitions_count(self):
        """Counts all nondeterministic transitions from each state"""
        self.nondeterministic_transitions_count = Counter()
        for s1, pairs in self.transitions.items():
            used_alphas = set()
            for a, s2 in pairs:
                if a in used_alphas:
                    self.nondeterministic_transitions_count[s1] += 1
                else:
                    used_alphas.add(a)

    def check_nondeterminism(self):
        """Check FSA is nondeterministic or not"""
        # We just check our counter
        # if counter is not empty - we have nondeterministic states - then we add warning
        if self.nondeterministic_transitions_count:
            self.warnings.add(3)

    def check_completeness(self):
        """Check FSA completeness"""
        alphabet_cardinality = len(self.alphabet)

        # for every state we compare sum of nondeterministic transitions and alphabet cardinality
        # with real transitions from this state
        # if they don't equal - then we have incomplete FSA
        for s1, pairs in self.transitions.items():
            if len(pairs) != self.nondeterministic_transitions_count[s1] + alphabet_cardinality:
                self.is_complete = False
                break

    def check(self):
        """Checking FSA"""
        self.get_nondeterministic_transitions_count()
        self.check_unreachable_and_disjoint_states()
        self.check_nondeterminism()
        self.check_completeness()

    def save(self):
        """Save FSA status if no errors"""
        if self.is_complete:
            status = "FSA is complete"
        else:
            status = "FSA is incomplete"
        if self.warnings:
            warnings = list(self.warnings)
            warnings.sort()
            status += "\nWarning:\n{}".format("\n".join([self.WARNINGS[i] for i in warnings]))
        self.write_status(status)


def main():
    fsa = FSA()  # create FSA object
    fsa.load()   # load FSA from file (we check some errors while loading)
    fsa.check()  # check E2, warnings, completeness
    fsa.save()   # save checking result if no errors


if __name__ == "__main__":
    main()
